//
//  Forecast.swift
//  RonnyWeather
//
//  Created by Ronny Setiawan on 31/05/18.
//  Copyright © 2018 RonApp. All rights reserved.
//

import Foundation

struct Forecast {
    
    var temp_min: Float?
    var temp_max: Float?
    var description: String?
    
}
