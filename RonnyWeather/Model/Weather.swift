//
//  Weather.swift
//  RonnyWeather
//
//  Created by Ronny Setiawan on 30/05/18.
//  Copyright © 2018 RonApp. All rights reserved.
//

import Foundation

struct Weather {
    
    var id: Int?
    var description: String?
    var temp: String?
    
    var country: String?
    var name: String?
    
}
