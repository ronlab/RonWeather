//
//  Endpoint.swift
//  RonnyWeather
//
//  Created by Ronny Setiawan on 30/05/18.
//  Copyright © 2018 RonApp. All rights reserved.
//

import Foundation
import SwiftyJSON

// Base Openweather URL
let BASE_URL = "http://api.openweathermap.org/data/2.5/"
let APP_ID = "&appid="
let API_KEY = "81ab79ea4fd446f970ae3391ddc1159f"

// Alias for completion
typealias JSONHandler = (_ result: JSON?) -> ()
