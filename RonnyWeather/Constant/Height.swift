//
//  Height.swift
//  RonnyWeather
//
//  Created by Ronny Setiawan on 30/05/18.
//  Copyright © 2018 RonApp. All rights reserved.
//

import Foundation
import UIKit

// Height constant for table cell
let WEATHER_TABLE_CELL_HEIGHT = CGFloat(80)
