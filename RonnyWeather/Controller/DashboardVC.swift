//
//  DashboardVC.swift
//  RonnyWeather
//
//  Created by Ronny Setiawan on 30/05/18.
//  Copyright © 2018 RonApp. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation
import Toast_Swift
import NVActivityIndicatorView

class DashboardVC: UIViewController {
    
    @IBOutlet weak var areaSearch: UISearchBar!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var weatherLabel: UILabel!
    
    @IBOutlet weak var forecastTableView: UITableView!
    
    // View
    var nib = UINib()
    var date = Date()
    var cityId = String()
    
    var locationManager = CLLocationManager()
    var lat = Double()
    var lon = Double()
    
    // TableView
    var row = Int()
    
    // API
    var oneWeather = Weather()
    
    var oneForecast = Forecast()
    var allForecast = [Forecast]()
    
    var activityData = ActivityData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        registerNib()
        setLocationManager()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        locationAuthStatus()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

// View Functions

extension DashboardVC: UISearchBarDelegate {
    
    // Registering TableViewCell
    func registerNib() {
        nib = UINib(nibName: WEATHER_TABLE_CELL, bundle: nil)
        forecastTableView.register(nib, forCellReuseIdentifier: WEATHER_TABLE_CELL)
    }
    
    // Set UI for current weather
    func setWeather(current: Weather) {
        dateLabel.text = "Today, \(setCurrentDate())"
        
        tempLabel.text = "\(current.temp!)˚"
        weatherImage.image = UIImage(named: "\(current.description!)")
        cityLabel.text = "\(current.name!), \(current.country!)"
        weatherLabel.text = current.description
    }
    
    // Set / Refresh forecast table
    func setForecastTable() {
        forecastTableView.reloadData()
    }
    
    // Set format for date
    func setCurrentDate() -> String {
        let format = DateFormatter()
        format.dateFormat = "dd MMM yyyy"
        let stringDate = format.string(from: date)
        
        return stringDate
    }
    
    // Set format for days
    func setForecastDate(row: Int) -> String {
        var timeInterval = DateComponents()
        timeInterval.day = row + 1
        let futureDate = Calendar.current.date(byAdding: timeInterval, to: Date())!
        
        let format = DateFormatter()
        format.dateFormat = "EEEE"
        let stringDate = format.string(from: futureDate)
        
        return stringDate
    }
    
    // Search Bar
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.areaSearch.endEditing(true)
        
        if let city = searchBar.text {
            weatherCityAPI(city: city)
        }
    }
    
    // Dismiss keyboard when tap outside search bar
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}

// CoreLocation Functions

extension DashboardVC: CLLocationManagerDelegate {
    
    // Get authorization for location
    func setLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
    // Get location from device & call API
    func locationAuthStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            let currentLocation = locationManager.location!
            lat = currentLocation.coordinate.latitude
            lon = currentLocation.coordinate.longitude
            
            weatherLocationAPI(lat: lat, lon: lon)
        } else {
            locationAuthStatus()
        }
    }
    
}

// TableView Functions

extension DashboardVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allForecast.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        row = indexPath.row
        let cell = forecastTableView.dequeueReusableCell(withIdentifier: WEATHER_TABLE_CELL, for: indexPath) as! WeatherTC
        
        cell.weatherImage.image = UIImage(named: allForecast[row].description!)
        cell.descriptionLabel.text = allForecast[row].description
        
        cell.minTempLabel.text = "\(KtoC(temp: "\(allForecast[row].temp_min!)"))˚"
        cell.maxTempLabel.text = "\(KtoC(temp: "\(allForecast[row].temp_max!)"))˚"
        
        cell.dayLabel.text = setForecastDate(row: row)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return WEATHER_TABLE_CELL_HEIGHT
    }
    
}

// API Functions

extension DashboardVC {
    
    // Get weather data by location
    func weatherLocationAPI(lat: Double, lon: Double) {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        WeatherNetwork.instance.getWeatherByLocation(lat: lat, lon: lon) { (result) in
            guard let temp = result?["main"]["temp"].stringValue, !temp.isEmpty,
                let weather = result?["weather"][0]["description"].stringValue,
                let country = result?["sys"]["country"].stringValue,
                let name = result?["name"].stringValue,
                let lat = result?["coord"]["lat"].doubleValue,
                let lon = result?["coord"]["lon"].doubleValue else {
                    let message = result?["message"].stringValue
                    self.view.makeToast(message)
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    return
            }

            self.oneWeather.temp = self.KtoC(temp: temp)
            self.oneWeather.description = self.adjustCaption(caption: weather)
            self.oneWeather.name = name
            self.oneWeather.country = country

            self.setWeather(current: self.oneWeather)
            
            self.allForecast.removeAll()
            self.forecastLocationAPI(lat: lat, lon: lon)
        }
    }
    
    // Get weather data by city
    func weatherCityAPI(city: String) {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        WeatherNetwork.instance.searchWeatherByCity(city: city) { (result) in
            guard let temp = result?["main"]["temp"].stringValue, !temp.isEmpty,
                let weather = result?["weather"][0]["description"].stringValue,
                let country = result?["sys"]["country"].stringValue,
                let name = result?["name"].stringValue,
                let lat = result?["coord"]["lat"].doubleValue,
                let lon = result?["coord"]["lon"].doubleValue else {
                    let message = result?["message"].stringValue
                    self.view.makeToast(message)
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    return
            }

            self.oneWeather.temp = self.KtoC(temp: temp)
            self.oneWeather.description = self.adjustCaption(caption: weather)
            self.oneWeather.name = name
            self.oneWeather.country = country

            self.setWeather(current: self.oneWeather)
            
            self.allForecast.removeAll()
            self.forecastLocationAPI(lat: lat, lon: lon)
        }
    }
    
    // Get forecast data by location
    func forecastLocationAPI(lat: Double, lon: Double) {
        WeatherNetwork.instance.getForecastByLocation(lat: lat, lon: lon) { (result) in
            guard let lists = result?["list"].arrayValue, !lists.isEmpty else {
                let message = result?["message"].stringValue
                self.view.makeToast(message)
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                return
            }
            
            for list in lists {
                self.oneForecast.temp_min = list["main"]["temp_min"].floatValue
                self.oneForecast.temp_max = list["main"]["temp_max"].floatValue
                
                let description = list["weather"][0]["description"].stringValue
                self.oneForecast.description = self.adjustCaption(caption: description)
                
                self.allForecast.append(self.oneForecast)
            }
            
            self.setForecastTable()
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
        }
    }
    
}
