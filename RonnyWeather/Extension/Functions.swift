//
//  Functions.swift
//  RonnyWeather
//
//  Created by Ronny Setiawan on 30/05/18.
//  Copyright © 2018 RonApp. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    // Convert Kelvin to Celsius
    func KtoC(temp: String) -> String {
        let kelvin = Float(temp)!
        let celsius = kelvin - 273
        let temp = String(format: "%.1f", celsius)
        
        return temp
    }
    
    // Renaming caption for image
    func adjustCaption(caption: String) -> String {
        let capitalized = caption.capitalized
        return capitalized
    }
    
}
