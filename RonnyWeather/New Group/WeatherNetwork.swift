//
//  WeatherNetwork.swift
//  RonnyWeather
//
//  Created by Ronny Setiawan on 30/05/18.
//  Copyright © 2018 RonApp. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class WeatherNetwork {
    
    static let instance = WeatherNetwork()
    
    // Request weather JSON by City
    func searchWeatherByCity(city: String, complete: @escaping JSONHandler) {
        var encode = city.trimmingCharacters(in: .whitespacesAndNewlines)
        encode = encode.replacingOccurrences(of: " ", with: "+")
        let url = "\(BASE_URL)weather?q=\(encode)\(APP_ID)\(API_KEY)"
        
        var request = URLRequest(url: URL(string: url)!)
        request.timeoutInterval = 10
        request.httpMethod = "POST"
        
        Alamofire.request(request).responseJSON { (response) in
            if let weatherLocationResponse = response.result.value {
                complete(JSON(weatherLocationResponse))
            } else {
                print(response.error!)
            }
        }
    }
    
    // Request weather JSON by location
    func getWeatherByLocation(lat: Double, lon: Double, complete: @escaping JSONHandler) {
        let url = "\(BASE_URL)weather?lat=\(lat)&lon=\(lon)\(APP_ID)\(API_KEY)"
        
        var request = URLRequest(url: URL(string: url)!)
        request.timeoutInterval = 10
        request.httpMethod = "POST"
        
        Alamofire.request(request).responseJSON { (response) in
            if let weatherLocationResponse = response.result.value {
                complete(JSON(weatherLocationResponse))
            } else {
                print(response.error!)
            }
        }
        
    }
    
    // Request forecast JSON by location
    func getForecastByLocation(lat: Double, lon: Double, complete: @escaping JSONHandler) {
        let url = "\(BASE_URL)forecast?lat=\(lat)&lon=\(lon)&cnt=5&mode=json\(APP_ID)\(API_KEY)"
        
        var request = URLRequest(url: URL(string: url)!)
        request.timeoutInterval = 20
        request.httpMethod = "POST"
        
        Alamofire.request(request).responseJSON { (response) in
            if let forecastLocationResponse = response.result.value {
                complete(JSON(forecastLocationResponse))
            } else {
                print(response.error!)
            }
        }
    }
}
